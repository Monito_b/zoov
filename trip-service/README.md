# Zoov - Test Microservices in Go with Docker and MongoDB

Overview
========

Trip service is a service in golang which a mongodb common connection by grpc to the Bike service
to start a trip and update a bike status

Requirements
===========

* Docker 1.12
* Docker Compose 1.8

Build services
==============================
```
make build
```


Starting services
==============================

```
make run
```

Stoping services
==============================

```
docker <id> stop
```

Documentation
======================

## Trip Service

This Service will give you information about a trip. "Started at", "Ended at", Location, Positions

it allows:
    Start Trip
    End Trip (TO DEBUG)

