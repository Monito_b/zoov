package main

import (
	"log"
	"os"

	pb "gitlab.com/Monito_b/zoov/trip-service/proto/trip"
	bikeProto  "gitlab.com/Monito_b/zoov/bike-service/proto/bike"
	"github.com/micro/go-micro/cmd"
	"github.com/micro/go-micro"
)


const (
	defaultHost = "localhost:27017"
)

func main() {

	// Database host from the environment variables
	host := os.Getenv("DB_HOST")

	if host == "" {
		host = defaultHost
	}

	session, err := CreateSession(host)

	defer session.Close()

	if err != nil {

		log.Panicf("Could not connect to datastore with host %s - %v", host, err)
	}

	cmd.Init()

    // Define our flags
	// Create a new service. Optionally include some options here.
	srv := micro.NewService(

		// This name must match the package name given in your protobuf definition
		micro.Name("go.micro.srv.trip"),
		micro.Version("latest"),
	)

	bikeClient := bikeProto.NewBikeServiceClient("go.micro.srv.bike", srv.Client())

	// Init will parse the command line flags.
	srv.Init()

	// Register handler
	pb.RegisterTripServiceHandler(srv.Server(), &service{session, bikeClient})

	// Run the server
	if err := srv.Run(); err != nil {
		log.Println(err)
	}
}
