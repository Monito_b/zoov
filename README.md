# Zoov - Test Microservices in Go with Docker and MongoDB

Overview
========

Zoov is an example test which demonstrates the use of microservices for the connected bikes servies 

The proyect structure is based in the knowledge learned in the book: BUILDING MICROSERVICES: DESIGNING FINE-GRANED SYSTEMS by Sam Newman
and by [Ewan Valentine](https://ewanvalentine.io/microservices-in-golang-part-1/)


Requirements
===========

* Docker 1.12
* Docker Compose 1.8

Build services
==============================
```
docker-compose build
```


Starting services
==============================

```
docker-compose up
```

Stoping services
==============================

```
docker-compose stop
```

Documentation
======================

## Bike Service

This service will give you information about a bike. Latitude, Longitude, Status

it allows:
    Retrive one bike by his id
    Retrive allbikes
    Update BikeStatus
    Create Bike
    

## Trip Service

This Service will give you information about a trip. "Started at", "Ended at", Location, Positions

it allows:
    Start Trip
    End Trip (TO DEBUG)

## Zoov Client

This small client show you how to execute the clients of our service.
This is the model to implement in the gateway service to allow the Routes excecution 

Tree
====
.                         
├── README.md             
├── bike-service          
│   ├── Dockerfile        
│   ├── Makefile          
│   ├── README.md         
│   ├── datastore.go      
│   ├── handler.go        
│   ├── main.go           
│   ├── proto             
│   │   └── bike          
│   └── repository.go     
├── connect_to_mongo.sh   
├── docker-compose.yml    
├── trip-service          
│   ├── Dockerfile        
│   ├── Makefile          
│   ├── README.md         
│   ├── datastore.go      
│   ├── handler.go        
│   ├── main.go           
│   ├── proto             
│   │   └── trip          
│   └── repository.go     
└── zoov_client           
    ├── Dockerfile        
    ├── Makefile          
    ├── README.md         
    ├── bike.json         
    ├── cli.go            
    └── trip.json
    
    
