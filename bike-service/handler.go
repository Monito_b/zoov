package main

import (
	"log"

	pb "gitlab.com/Monito_b/zoov/bike-service/proto/bike"
	"golang.org/x/net/context"
	"gopkg.in/mgo.v2"
)

// Service should implement all of the methods to satisfy the service
// we defined in our protobuf definition. You can check the interface
// in the generated code itself for the exact method signatures etc
// to give you a better idea.
type service struct {
	session *mgo.Session
}

func (s *service) GetRepo() Repository {
	return &BikeRepository{s.session.Clone()}
}

// CreateBike - we created just one method on our service,
// which is a create method, which takes a context and a request as an
// argument, these are handled by the gRPC server.
func (s *service) CreateBike(ctx context.Context, req *pb.Bike, res *pb.Response) error {
	defer s.GetRepo().Close()

	if err := s.GetRepo().Create(req); err != nil {
		return err
	}
	res.Bike = req
	log.Printf("Bike created : %s \n", res.Bike.Id)
	return nil
}

func (s *service) GetBikes(ctx context.Context, req *pb.GetRequest, res *pb.Response) error {
	defer s.GetRepo().Close()
	bikes, err := s.GetRepo().GetAll()
	if err != nil {
		return err
	}
	res.Bikes = bikes
	return nil
}

func (s *service) GetBike(ctx context.Context, req *pb.Specification, res *pb.Response) error {
	defer s.GetRepo().Close()

	bike, err := s.GetRepo().Get(req)

	if err != nil {
		return err
	}
	res.Bike = bike
	log.Println(bike)
	return nil
}


func (s *service) UpdateStatusBike(ctx context.Context, req *pb.Specification, res *pb.Response) error {
	defer s.GetRepo().Close()

	updated, err := s.GetRepo().Update(req)

	if err != nil {
		return err
	}
	res.Available = updated
	return nil
}

