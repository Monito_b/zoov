# Zoov - Test Microservices in Go with Docker and MongoDB

Overview
========

Bike is a service to retrive and modify bikes data in Mongodb. For the case of example we are gonna centralise only in one DataBase but we could use another connection

Requirements
===========

* Docker 1.12
* Docker Compose 1.8

Build services
==============================

```
Make build
```


Starting services
==============================

```
make run
```

Stoping services
==============================

```
CTRL-C
Docker stop
```

Note: If you want to comunicate with the other services it should be running in dockerland with docker-compose.

	But in standalone should be running mongodb in port default

```
docker-compose build bike-sercie
```

Documentation
======================

## Bike Service

This service will give you information about a bike. Latitude, Longitude, Status

it allows:
    Retrive one bike by his id
    Retrive allbikes
    Update BikeStatus
    Create Bike
    
