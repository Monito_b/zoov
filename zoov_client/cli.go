package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	microclient "github.com/micro/go-micro/client"
	"github.com/micro/go-micro/cmd"
	pb "gitlab.com/Monito_b/zoov/bike-service/proto/bike"
	tpb "gitlab.com/Monito_b/zoov/trip-service/proto/trip"
	"golang.org/x/net/context"
)

const (
	defaultFilename = "bike.json"
	tripFileName = "trip.json"
)

func parseFile(file string) ([]*pb.Bike, error) {
	var bikes []*pb.Bike
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(data, &bikes); err != nil {
		log.Fatalf("Failed to load json: %v", err)
	}

	return bikes, err
}

func parseTripFile(file string) ([]*tpb.Bike, error) {
	var bikes []*tpb.Bike
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(data, &bikes); err != nil {
		log.Fatalf("Failed to load trip json file %v", err)
	}
	return bikes, err
}

func main() {

	cmd.Init()

	// Create new Bike client
	client := pb.NewBikeServiceClient("go.micro.srv.bike", microclient.DefaultClient)
	TripClient := tpb.NewTripServiceClient("go.micro.srv.trip", microclient.DefaultClient)

	// Contact the server and print out its response.
	BikeFile := defaultFilename
	TripFile := tripFileName
	if len(os.Args) > 1 {
		BikeFile = os.Args[1]
		TripFile = os.Args[2]
	}
	bikes, err := parseFile(BikeFile)

	if err != nil {
		log.Fatalf("Could not parse bike file: %v", err)
	}

	trips, err := parseTripFile(TripFile)

	if err != nil {
		log.Fatalf("Could not parse Trip file: %v", err)
	}

	for _, bike := range bikes {
		_, err := client.CreateBike(context.TODO(), bike)
		if err != nil {
			log.Println("Could not create: %v", err)
		}
		log.Println(bike)
	}


	getAll, err := client.GetBikes(context.Background(), &pb.GetRequest{})
	if err != nil {
		 log.Println("Could not list bikes:", err)
	}
	for _, v := range getAll.Bikes {
		log.Println("Bike from list:  %v", v)
	}

	getBike, err := client.GetBike(context.Background(), &pb.Specification{Id: "bike001"})
	if err != nil {
	 	log.Printf("Could not find bikes: %v", err)
	}

	log.Println("Bike by id: ", getBike.Bike.Id, getBike.Bike.Status, getBike.Bike.Location.Coordinates.Latitude)


	for _, Biketrip := range trips {
		id, err := TripClient.StartTrip(context.TODO(), Biketrip)
		if err != nil {
			log.Printf("Could not create trip: %v", err)
		}
		log.Printf("Trip created with id [%v]", id)
	}
}
